package de.higger.freewartoolset.master_data.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;


@Configuration
@EnableWebSecurity
public class AuthConfiguration extends WebSecurityConfigurerAdapter {

	@Value("${application.ws.apikey}")
	private String apikey;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		ApiKeyAuthenticationFilter authenticationFilter = new ApiKeyAuthenticationFilter();
		authenticationFilter.setAuthenticationManager(new ApiKeyAuthenticationManager(apikey));

		
		http
			.antMatcher("/api/**").
        	csrf().disable()
        	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .addFilter(authenticationFilter)
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/api/**").permitAll()
            .anyRequest()
            .authenticated();
	}
}
