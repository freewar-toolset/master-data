
package de.higger.freewartoolset.master_data.configuration;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.AuthorizationScopeBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("de.higger.freewartoolset.master_data"))
				.paths(PathSelectors.any())
				.build()
				.securitySchemes(Collections.singletonList(apikey()))
				.securityContexts(Collections.singletonList(securityContext()));
	}

	private ApiKey apikey() {
		return new ApiKey("Authorization-Key", ApiKeyAuthenticationFilter.HEADER_NAME_APIKEY, "header");
	}

	private SecurityContext securityContext() {

		AuthorizationScope[] authScopes = new AuthorizationScope[1];
		authScopes[0] = new AuthorizationScopeBuilder().scope("global")
				.description("full access")
				.build();
		SecurityReference securityReference = SecurityReference.builder()
				.reference("Authorization-Key")
				.scopes(authScopes)
				.build();

		return SecurityContext.builder()
				.securityReferences(Collections.singletonList(securityReference))
				.build();
	}

}
