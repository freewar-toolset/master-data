package de.higger.freewartoolset.master_data.domain;

import java.util.Optional;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Waffe {

	@NonNull
	private String link;

	@NonNull
	private String bezeichnung;

	@NonNull
	private Range starke;

	private Optional<Range> mindeststaerke = Optional.empty();

	private Optional<Range> mindestakademielimit = Optional.empty();

	private Optional<Range> mindestintelligenz = Optional.empty();

	private Optional<Rasse[]> notwendigeRasse = Optional.empty();
}
