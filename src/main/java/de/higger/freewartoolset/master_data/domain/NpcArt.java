package de.higger.freewartoolset.master_data.domain;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

public enum NpcArt {

	STANDARD_NPC("NPC"), UNIQUE_NPC("Unique-NPC"), GRUPPEN_NPC("Gruppen-NPC"), INVASIONS_NPC("Invasions-NPC"),
	RESISTENZ_NPC("Resistenz-NPC"), SUPERRESISTENZ_NPC("Superresistenz-NPC");

	public static final String ARTEN_PATTERN = Arrays.stream(NpcArt.values())
			.map(NpcArt::getBezeichnung)
			.collect(Collectors.joining("|"));

	private String bezeichnung;

	NpcArt(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public static Optional<NpcArt> byBezeichnung(String bezeichnung) {
		return Arrays.stream(NpcArt.values())
				.filter(npcArt -> npcArt.getBezeichnung().equals(bezeichnung))
				.findAny();
	}

}
