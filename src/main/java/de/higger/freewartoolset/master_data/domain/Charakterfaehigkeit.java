package de.higger.freewartoolset.master_data.domain;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.NonNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Charakterfaehigkeit {

	@NonNull
	private String link;

	@NonNull
	private String bezeichnung;

	@NonNull
	private Integer lernzeit;

	@NonNull
	private Integer maximalstufe;

	private Optional<String> berechnung = Optional.empty();
}
