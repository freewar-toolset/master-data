package de.higger.freewartoolset.master_data.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.higger.freewartoolset.master_data.domain.Waffe;

@RestController
@RequestMapping("/api/angriffswaffe")
public class AngriffswaffeResource {

	private static Map<String, Waffe> angriffswaffen = new HashMap<>();

	@PutMapping
	public void aktualisiereAngriffswaffe(@RequestBody Waffe waffe) {
		angriffswaffen.put(waffe.getLink(), waffe);
	}

	@GetMapping("/{bezeichnung}")
	public Optional<Waffe> getAngriffswaffe(String bezeichnung) {
		return angriffswaffen.values()
				.stream()
				.filter(w -> w.getBezeichnung()
						.toLowerCase()
						.equals(bezeichnung.toLowerCase()))
				.findAny();
	}

	@GetMapping
	public Collection<Waffe> getAngriffswaffen() {
		return angriffswaffen.values();
	}
}
