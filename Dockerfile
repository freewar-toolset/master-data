FROM openjdk:14-alpine

WORKDIR /application
COPY target/unzip/BOOT-INF/lib /application/lib
COPY target/unzip/META-INF /application/META-INF
COPY target/unzip/BOOT-INF/classes /application

CMD ["sh","-c","java -cp .:lib/* de.higger.freewartoolset.master_data.MasterDataApplication"]
